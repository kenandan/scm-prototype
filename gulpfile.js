var gulp = require('gulp')
var uglify = require('gulp-uglify')
var source = require('vinyl-source-stream')
var browserify = require('browserify')
var watchify = require('watchify')
var reactify = require('reactify')
var buffer = require('vinyl-buffer');
var clean = require('gulp-clean')

var path = {
	HTML : 'src/theme.html',
	MINIFIED_OUT : 'build.min.js',
	OUT : 'build.js',
	DEST : 'dist',
	DEST_BUILD : 'dist/build',
	DEST_SRC : 'dist/src',
	ENTRY_POINT : './src/components/Main.jsx'
}

gulp.task('copy', function(){
	gulp.src(path.HTML)
		.pipe(path.DEST);
});

gulp.task('clean', function(){
	gulp.src(path.DEST_BUILD, {read: false})
	.pipe(clean())
});

gulp.task('build', ['clean'], function(){
	browserify({
		entries: path.ENTRY_POINT,
		transform: [reactify]

	})
	.bundle()
	.pipe(source(path.OUT))
	//	.pipe(buffer()) // Using vinyl-buffer since uglify only works with buffered vinyl file objects
	//.pipe(uglify())
	.pipe(gulp.dest(path.DEST_BUILD))
	.on('error', function(err){
			console.log('The error in my case is ' + err.message);
		});
})

gulp.task('watch', function(){
	gulp.watch(path.HTML, ['copy']);
	var watcher = watchify(browserify({
		entries: [path.ENTRY_POINT],
		transform: [reactify],
		debug: true,
		cache: {},
		packageCache: {},
		fullPaths: true
	}));

	return watcher.on('update', function(){
		watcher.bundle()
		.pipe(source(path.OUT))
		.pipe(gulp.dest(path.DEST_BUILD))
		console.log('Updated');
	})
		.bundle()
		.pipe(source(path.OUT))
		.pipe(gulp.dest(path.DEST_BUILD));

});

gulp.task('default', ['watch']);
