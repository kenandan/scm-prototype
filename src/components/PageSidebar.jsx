var React = require('react');
var SideBar = require('./SideBarComp.jsx');

var PageSidebar = React.createClass({

    render: function(){
        return(
            <div><SideBar /></div>
        )
    }
});

module.exports = PageSidebar;