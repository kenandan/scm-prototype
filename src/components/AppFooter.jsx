var React = require('react')
var AppFooter = React.createClass({
    render: function(){
        return(
            <div className="page-footer">
                <div className="page-footer-inner">
                    2014 &copy; Metronic by keenthemes. <a
                    href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
                    title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase
                    Metronic!</a>
                </div>
                <div className="scroll-to-top">
                    <i className="icon-arrow-up"></i>
                </div>
            </div>
        )
    }
})

module.exports = AppFooter;