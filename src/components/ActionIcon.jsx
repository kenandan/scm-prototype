var React = require('react');

module.exports = React.createClass({
    render: function(){
        return(
            <a href="javascript:;" className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true">
                <i className={this.props.icon}></i>
                <span className="badge badge-default">{this.props.badge}</span>
            </a>
        )
    }
});