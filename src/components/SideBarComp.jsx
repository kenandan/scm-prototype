var React = require('react')

/* Data to drive the sideBar */

window.NodeData = [
		{title: "Dashboard", icon: "icon-home", arrow: "arrow",
			child: [{title: "Default Dashboard", icon: "icon-bar-chart"},
					{title: "New Dashboard #1", icon: "icon-bulb"},
					{title: "New Dashboard #2", icon: "icon-graph"}
			]
		},
		{title: "eCommerce", icon: "icon-basket", arrow: "arrow"},
		{title: "Page Layouts", icon: "icon-rocket", arrow: "arrow"},
		{title: "UI Features", icon: "icon-diamond", arrow: "arrow", 
			child: [{title: "General Components", icon: "icon-bar-chart"},
					{title: "Buttons", icon: "icon-bulb"},
					{title: "Popover Confirmations", icon: "icon-graph"}
			]
		}
	];





/* Child node class */
var ChildNode = React.createClass({
	  getDefaultProps : function() {
    		return {
      			"icon" : ""
      		};
  		},
	  render: function() {
	    return (
	      <li>
	      	<a href="index.html" style={{color:"#959fae"}}>
                <i className={this.props.icon}></i>
                    {this.props.title}
            </a>
	      </li>
	    );
	  }
	});

/* Nested sideBarNode rendering */
var ChildSideBarList = React.createClass({
	  render: function() {
	    var ChildSideBarNodes = this.props.data.map(function(node, index){
	    	return (
	    		<ChildNode
	    			key={index}
	    			title={node.title}
	    			icon={node.icon} />
	    	);
	    });
	    return (
	      <div>
	      	{ChildSideBarNodes}
	      </div>
	    );
	  }
	});

/* Parent Node */
var Node = React.createClass({
		getDefaultProps : function() {
    		return {
      			"arrow" : ""
      		};
  		},
	  render: function() {
	  	var childs = [];
	  	var arrow = "";
        if (this.props.child) {
            childs = <ChildSideBarList data={this.props.child} />;
            arrow = this.props.arrow;
        }
	    return (
	      <li className="start ">
	      	<a href="javascript:;" style={{color:"#959fae"}}>
                <i className={this.props.icon}></i>
                {this.props.title}
                <span className={arrow}></span>
	      	</a>
	      	<ul className="sub-menu">
	      		{childs}
	      	</ul>
	      </li>
	    );
	  }
	});

/* Opens sideBar list and creates the components */
var SideBarList = React.createClass({
	  render: function() {
	    var sideBarNodes = this.props.data.map(function(node, index){
	    	return (
	    		<ul className="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true"
                        data-slide-speed="100">
	    			<Node
	    				key={index}
	    				title={node.title}
	    				child={node.child}
	    				icon={node.icon}
	    				arrow={node.arrow} />
	    		</ul>
	    	);
	    })
	    return (
	      <div>
	      	{sideBarNodes}
	      </div>
	    );
	  }
	});

/* Main Component which receives the side-bar data from API */ 
var SideBar = React.createClass({
	  render: function() {
      /* pass the received prop into the child component */
	    return (
	      <div className="page-sidebar-wrapper">
	      	<div className="page-sidebar navbar-collapse collapse">
	        	<SideBarList data={window.NodeData}/>
	        </div>
	      </div>
	    );
	  }
	});

module.exports = SideBar








