var React = require('react');
var Header = require('./ActionHeader.jsx');
var List = require('./ReactList.jsx');

var ActionContainer = React.createClass({

    getDefaultProps: function(){
        return {
            listItemClass: "dropdown-menu"
        }
    },

    render: function(){

        var itemList = React.Children.map(this.props.children, function(child){
            return <li>{child}</li>
        });

        return(
            <ul className={this.props.listItemClass}>
                {itemList}
            </ul>
        );
    }
});

module.exports = ActionContainer;