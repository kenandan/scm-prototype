var React = require('react');
var UserProfile = React.createClass({
    render: function(){

        var item = this.props.item;
        var span = item.span;
        var linkClass = item.linkClass;

        if(item.span){
            return(
                <li className={linkClass}>
                    <a href={item.link}>
                        <i className={item.icon}></i>
                        {item.content}
                        <span className={span.class}>{span.value}</span>
                    </a>
                </li>
            )
        }
        else{
            return(
                <li className={linkClass}>
                    <a href={item.link}>
                        <i className={item.icon}></i>
                        {item.content}
                    </a>
                </li>
            )
        }
    }
});

module.exports = UserProfile;