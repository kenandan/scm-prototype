var React = require('react');
var Reflux = require('reflux');
var AppHeader = require('./AppHeader.jsx');
var AppFooter = require('./AppFooter.jsx');
var PageSidebar = require('./PageSidebar.jsx');
var PageContent = require('./PageContent.jsx');
var List = require('./ReactList.jsx');
var ActionContainer = require('./ActionContainer.jsx');
var Message = require('./InboxMessage.jsx');
var Notification = require('./NotificationMessage.jsx');
var Inbox = require('./InboxMessage.jsx');
var Todo = require('./TodoMessage.jsx');
var Login = require('./LoginMessage.jsx');
var Header = require('./ActionHeader.jsx');
var ActionDropDown = require('./ActionDropdown.jsx');
var Logo = require('./AppLogo.jsx');
var ActionIcon = require('./ActionIcon.jsx');
var LoginIcon = require('./LoginIcon.jsx');
var LoginActionDropDown = require('./AppLoginComp.jsx');
var NotificationData = require('../data/notification');
var InboxData = require('../data/inbox');
var TodoData = require('../data/todo');
var LoginData = require('../data/login');
var NotificationStore = require('../stores/NotificationStore.jsx');

var AppBody = React.createClass({
    mixins: [Reflux.connect(NotificationStore, 'notification')],

    render: function () {

        return(
            <AppHeader>
                <ActionDropDown dropDownClass={NotificationData.dropDownClass}>
                    <ActionIcon icon={NotificationData.icon} badge="7" />
                    {/* A container further contains a Header and a List of messages*/}
                    <ActionContainer>
                        <Header data={NotificationData.title}/>
                        {/* The Message List takes the data and renderer component from its props */}
                        <List data={NotificationData.data} renderer={Notification}/>
                    </ActionContainer>
                </ActionDropDown>
                <ActionDropDown dropDownClass={InboxData.dropDownClass}>
                    <ActionIcon icon={InboxData.icon} badge="7" />
                    <ActionContainer>
                        <Header data={InboxData.title}/>
                        <List data={InboxData.data} renderer={Inbox}/>
                    </ActionContainer>
                </ActionDropDown>
                <ActionDropDown dropDownClass={TodoData.dropDownClass}>
                    <ActionIcon icon={TodoData.icon} badge="7" />
                    <ActionContainer>
                        <Header data={TodoData.title}/>
                        <List data={TodoData.data} renderer={Todo}/>
                    </ActionContainer>
                </ActionDropDown>

                <ActionDropDown  dropDownClass="dropdown dropdown-user">
                    <LoginIcon username="Nick" />
                    <List listItemClass="dropdown-menu dropdown-menu-default" data={LoginData} renderer={Login} />
                </ActionDropDown>
            </AppHeader>
        )
    }
});

module.exports = AppBody;