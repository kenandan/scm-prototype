/*
* A common pattern is to create several stateless components that just render data,
* and have a stateful component above them in the hierarchy that passes its state to its children via props.
* The stateful component encapsulates all of the interaction logic,
* while the stateless components take care of rendering data in a declarative way.
* */
var React = require('react');

var ActionItemList = React.createClass({

    getDefaultProps(){
        return {
            listItemClass: "dropdown-menu-list"
        }
    },

    getInitialState(){
        {/* Set the state that stores information for MessageList */}
        return null;
    },

    render: function(){

        var Component = this.props.renderer;

        /* myList is the list of renderer components computed using data from the props*/
        var myList = this.props.data.map(function(item, id){
            return <Component item={item}/>
        });

        return (
            <ul className={this.props.listItemClass} styles="height: 250px;"
                    data-handle-color="#637283">
                {myList}
            </ul>
        )
    }
});

module.exports = ActionItemList;