var React = require('react')
var PageContent = React.createClass({
    render : function(){
        return(
            <div className="page-content-wrapper">
                <div className="page-content">
                    <h3 className="page-title"> Blank Page </h3>
                    <div className="row">
                        <div className="col-md-12">
                            Page content goes here
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})

module.exports = PageContent;
