var React = require('react');
var InboxMessage = React.createClass({

    render: function(){

        var item = this.props.item;

        return(
            <li>
                <a href="inbox.html?a=view">
                    <span className="photo">
                        <img src={item.image} className="img-circle" alt="" />
                    </span>
                    <span className="subject">
                        <span className="from">
                            {item.from}
                        </span>
                        <span className="time">
                            {item.time}
                        </span>
                    </span>
                    <span className="message">
                        {item.message}
                    </span>
                </a>
            </li>
        )
    }
});

module.exports = InboxMessage;