var React = require('react');
var NotificationStore = require('../stores/NotificationStore.jsx');
var NotificationActions = require('../actions/NotificationActions.jsx');

var Notification = React.createClass({

    //mixins: [Reflux.connect(NotificationStore, 'notification')],

    removeNotification: function(){
        console.log('action called with '+this.props.item);
        NotificationActions.readNotification(this.props.item);
    },

    render: function(){
        var item = this.props.item;
        var label = "label label-sm label-icon label-" + item.level;
        var iconClass = "fa fa-" + item.iclass;

        return(
            <li onClick={this.removeNotification}>
                <a href="javascript:;">
                    <span className="time" style={{'float':'right'}}>{item.time}</span>
                    <span className="details">
                        <span className={label}>
                            <i className={iconClass} ></i>
                        </span>
                        {item.content}
                    </span>
                </a>
            </li>
        )
    }
});

module.exports = Notification;