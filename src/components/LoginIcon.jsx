var React = require('react');


module.exports = React.createClass({

    getDefaultProps(){
        return {
            icon : "icon-user"
        }
    },

    render: function(){
        return(
            <a href="javascript:;" className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true">
                <i className={this.props.icon}></i>
                <span className="username username-hide-on-mobile">{this.props.username}</span>
                <i className="fa fa-angle-down"></i>
            </a>
        )
    }
});