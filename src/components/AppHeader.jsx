var React = require('react');
var DropDownMessageList = require('./ReactList.jsx');
var Container = require('./ActionContainer.jsx');
var Message = require('./InboxMessage.jsx');
var Notification = require('./NotificationMessage.jsx');
var Inbox = require('./InboxMessage.jsx');
var Todo = require('./TodoMessage.jsx');
var Login = require('./LoginMessage.jsx');
var Header = require('./ActionHeader.jsx');
var DropDown = require('./ActionDropdown.jsx');
var Icon = require('./ActionIcon.jsx');
var LoginDropDown = require('./AppLoginComp.jsx');
var Logo = require('./AppLogo.jsx');

var AppHeader = React.createClass({

    render: function(){

        return(
            <div className="page-header md-shadow-z-1-i navbar navbar-fixed-top">
                <div className="page-header-inner">
                    <Logo />
                    <a href="javascript:;" className="menu-toggler responsive-toggler" data-toggle="collapse"
                       data-target=".navbar-collapse">
                    </a>
                    <div className="top-menu">
                        <ul className="nav navbar-nav pull-right">
                            {this.props.children}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = AppHeader;