var React = require('react');

module.exports = React.createClass({
    render: function(){
        return(
            <div className="page-logo">
                <a href="index.html">
                    <h3>SCM APP</h3>
                </a>
                <div className="menu-toggler sidebar-toggler hide">
                </div>
            </div>
        )
    }
});