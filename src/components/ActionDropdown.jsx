var React = require('react');

/* ActionDropDown class which returns a list of its child components */
module.exports = React.createClass({

    getDefaultProps: function(){
        return {
            dropDownClass: "dropdown dropdown-extended"
        }
    },

    render: function(){

        return(
            <li className={this.props.dropDownClass}>
                {this.props.children}
            </li>
        )
    }
});