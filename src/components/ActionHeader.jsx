var React = require('react');

module.exports = React.createClass({

    render: function(){
        var item = this.props.data;
        return (
            <div className="external">
                <h5>{item.content}</h5>
                <a href={item.link}>view all</a>
            </div>
        )
    }
});