var React = require('react');
var Component = require('./ReactList.jsx');
var Icon = require('./ActionIcon.jsx');
var Login = require('./LoginMessage.jsx');

module.exports = React.createClass({

    render: function() {
        return (
            <li className="dropdown dropdown-user">
                <Icon icon="icon-user">
                    <span className="username username-hide-on-mobile">{this.props.username}</span>
                    <i className="fa fa-angle-down"></i>
                </Icon>
                <Component listItemClass="dropdown-menu dropdown-menu-default" data={this.props.data} renderer={Login} />
            </li>
        )
    }
});