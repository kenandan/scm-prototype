var React = require('react')

var TodoTask = React.createClass({
    render: function(){

        var item = this.props.item;
        var percent = item.percent + "%";
        var progress = item.progress + "% Complete";
        var spanStyle = "width: " + item.progress + "%;";

        return(
            <li>
                <a href="javascript:;">
                    <span className="task">
                        <span className="desc">{item.content}</span>
                        <span className="percent">{percent}</span>
                    </span>
                    <span className="progress">
                        <span styles={spanStyle} className="progress-bar progress-bar-success"
                              aria-valuenow={item.progress} aria-valuemin="0" aria-valuemax="100">
                            <span className="sr-only">{progress}</span>
                        </span>
                    </span>
                </a>
            </li>
        )
    }
});

module.exports = TodoTask;