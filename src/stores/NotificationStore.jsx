var Reflux = require('reflux');
var NotificationActions = require('../actions/NotificationActions.jsx');

var data = require('../data/notification.js').data;

module.exports = Reflux.createStore({
    listenables: [NotificationActions],
    getInitialState: function(){
        return data;
    },
    init(){

    },
    onReadNotification: function(item){
        //console.log('item is '+item);
        var index = data.indexOf(item);
        data.splice(index, 1);
        this.trigger(data);
    }
});