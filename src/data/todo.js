/**
 * Created by 11043 on 21/07/15.
 */
var data = [
    {content: "New release v1.2", percent: "30", progress: "40"},
    {content: "Application deployment", percent: "65", progress: "65"},
    {content: "Mobile app release", percent: "98", progress: "98"},
    {content: "Database migration", percent: "10", progress: "10"},
    {content: "Web server upgrade", percent: "58", progress: "58"},
    {content: "Mobile development", percent: "85", progress: "85"},
    {content: "New UI release", percent: "38", progress: "38"}
];

var title = {
    content: "You have 12 pending tasks",
    link: "page_todo.html"
};

module.exports = {
    dropDownClass: "dropdown dropdown-extended dropdown-tasks",
    icon: "icon-calendar",
    data: data,
    title: title
};