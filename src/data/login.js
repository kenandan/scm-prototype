/**
 * Created by 11043 on 21/07/15.
 */
module.exports = [
    {link: "extra_profile.html", icon: "icon-user", content: "My Profile"},
    {link: "page_calendar.html", icon: "icon-calendar", content: "My Calendar"},
    {link: "inbox.html", icon: "icon-envelope-open", content: "My Inbox", span: {class: "badge badge-danger", value: "3"}},
    {link: "page_todo.html", icon: "icon-rocket", content: "My Tasks", span: {class: "badge badge-success", value: "7"}},
    {linkClass: "divider"},
    {link: "extra_lock.html", icon: "icon-lock", content: "Lock Screen"},
    {link: "login.html", icon: "icon-key", content: "Log Out"}
];