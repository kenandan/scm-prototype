var data = [
    {img: "../../assets/admin/layout3/img/avatar2.jpg", from: "Lisa Wong", time: "Just Now", message: "Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh..."},
    {img: "../../assets/admin/layout3/img/avatar3.jpg", from: "Richard Doe", time: "16 mins", message: "Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh..."},
    {img: "../../assets/admin/layout3/img/avatar1.jpg", from: "Bob Nilson", time: "2 hrs", message: "Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh..."},
    {img: "../../assets/admin/layout3/img/avatar2.jpg", from: "Lisa Wong", time: "40 mins", message: "Vivamus sed auctor 40% nibh congue nibh..."},
    {img: "../../assets/admin/layout3/img/avatar3.jpg", from: "Richard Doe", time: "46 mins", message: "Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh..."}
];

var title = {
    content: "You have 7 New Messages",
    link: "page_inbox.html"
};

var InboxData = {
    dropDownClass: "dropdown dropdown-extended dropdown-inbox",
    icon: "icon-envelope-open",
    title: title,
    data: data
};

module.exports = InboxData;