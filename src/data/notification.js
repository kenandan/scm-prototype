var data = [
    {time : "just now", level : "success", iclass : "plus", content : "New user registered."},
    {time : "3 mins", level : "danger", iclass : "bolt", content : "Server #12 overloaded."},
    {time : "10 mins", level : "warning", iclass : "bell-o", content : "Server #2 not responding."},
    {time : "14 hrs", level : "info", iclass : "bullhorn", content : "Application error."},
    {time : "2 days", level : "danger", iclass : "bolt", content : "Database overloaded 68%."},
    {time : "3 days", level : "danger", iclass : "bolt", content : "A user IP blocked. "},
    {time : "4 days", level : "warning", iclass : "bell-o", content : "Storage Server #4 not responding dfdfdfd."},
    {time : "5 days", level : "info", iclass : "bullhorn", content : "System Error."},
    {time : "9 days", level : "danger", iclass : "bolt", content : "Storage server failed."}
];

var title = {
    content: "12 pending notifications",
    link: "extra_profile.html"
};

var NotificationData = {
    dropDownClass: "dropdown dropdown-extended",
    icon: "icon-bell",
    title: title,
    data: data
};

module.exports = NotificationData;